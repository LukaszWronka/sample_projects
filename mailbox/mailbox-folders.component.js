(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('mailboxFolders', {
            templateUrl: 'app/partials/mailbox/mailbox-folders.view.html',
            controller: controller,
            bindings : {
                unread : "=",
                folders : "=",
                accountId : "@"
            }
        });

    controller.$inject = ['$q', '$scope', '$uibModal', 'toaster','alertService','mailboxService'];
    function controller($q,$scope,$uibModal,toaster,alertService,mailboxService) {
        var ctrl = this;

        this.$onInit = function () {
            _.each(ctrl.folders,function (folder) {
                mailboxService
                    .getUnreadCount(folder.id)
                    .then(assignUnreadCountToFolder.bind(null,folder));
            });

        };

        function assignUnreadCountToFolder(folder, count) {
            if(!_.isUndefined(count) && count)
            {
                folder.unreadCount = count;
            }

        }
    }
}());
