(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('mailboxMessageCompose', {
            templateUrl: 'app/partials/mailbox/mailbox-message-compose.view.html',
            controller: controller,
            bindings : {
                detail : "<",
                addressBook : "<",
                emailAccountsMailboxes : '<',
                activeEmailAccount : '<',
                userClients: '<'
            }
        });

    controller.$inject = ['$q','$filter','$timeout', '$scope', '$state','$uibModal','tinymceConfigurationService', 'toaster','alertService','templateService','mailboxService','LoggedUserService','$stateParams','previewModalService'];
    function controller($q, $filter, $timeout, $scope, $state, $uibModal, tinymceConfigurationService,toaster,alertService,templateService,mailboxService,LoggedUserService,$stateParams,previewModalService) {
        var ctrl = this;

        ctrl.discard = discard;
        ctrl.send = send;
        ctrl.useTemplate = useTemplate;
        ctrl.addAttachments = addAttachments;
        ctrl.openComponentModalSend = openComponentModalSend;
        ctrl.openComponentModal = openComponentModal;
        ctrl.openPreview = openPreview;

        ctrl.tinymceOptionsDefault = tinymceConfigurationService.get('default');
        ctrl.addressBookFlat  = [];
        ctrl.clientBookFlat = [];
        ctrl.toCcInput = false;
        ctrl.checkParam = false;


        this.$onInit = function () {
            prepareDetailMailForm();
            getClientsEmailsList()
                .then(prepareClientsContactList);
        };

        function prepareClientsContactList(clientsContact) {
            ctrl.clientBookFlat = _.flatten(clientsContact);
            console.log(ctrl.clientBookFlat,ctrl.clientBookFlat);
        }



        function getClientsEmailsList() {
            var promises = [];

            if(ctrl.userClients.length>0) {
                _.each(ctrl.userClients,function (client) {

                    var defer = $q.defer();
                    promises.push(defer.promise);
                    mailboxService.getClientEmailsList_CUSTOM(client.id)
                        .then(function (emailsList) {
                            defer.resolve(emailsList.plain());
                            return emailsList;
                        })
                        .catch(function (error) {
                            defer.resolve([]);
                            return error;
                        })
                })
            }
            return $q.all(promises);
        }

        function prepareDetailMailForm() {
            if(isReplayState()) {
                ctrl.detail.subject="RE: "+ctrl.detail.subject;
                ctrl.detail.contentHtml= createReplayBodyArchive(ctrl.detail.contentHtml);
                ctrl.detail.toArray = [{name : ctrl.detail.fromName, email: ctrl.detail.fromAddress }];


            }
            if(isForwardState()) {
                ctrl.detail.subject="FW: "+ctrl.detail.subject;
                ctrl.detail.contentHtml= createReplayBodyArchive(ctrl.detail.contentHtml);
                ctrl.detail.toArray = [];


            }
            if(isComposeState()) {
                ctrl.detail={};
                ctrl.detail.attachmentsCollection = [];

            }
            ctrl.detail.isSent = false;
        }

        function addAttachments() {
            var attachmentModalInstance = $uibModal.open({
                animation:  true,
                component: 'attachmentsPopup',
                size :'lg',
                resolve: {
                    attachmentsCollection: function () {
                        return ctrl.detail.attachmentsCollection;
                    },
                    recipientsArray : function () {
                        return ctrl.detail.toArray;
                    }
                }
            });

            attachmentModalInstance.result.then(function (attachmentsCollection) {
                console.log('result then',attachmentsCollection);
                ctrl.detail.attachmentsCollection = attachmentsCollection;
            }, function () {
                console.log('modal-component dismissed at: ' + new Date());
            });
        }

        function getMailboxResource(type) {
            console.log('LoggedUserService.getLoggedUser().getActiveEmailAccount()',LoggedUserService.getLoggedUser().getActiveEmailAccount());
            var activeEmailAccount = LoggedUserService.getLoggedUser().getActiveEmailAccount();
           // var selectedEmailAccount = _.find(ctrl.emailAccountsMailboxes, {id: ctrl.activeEmailAccount.id});
            return _.find(activeEmailAccount.mailboxes, {mailboxType: type})._links.self.href;
        };


        function useTemplate() {
                var modalInstance = $uibModal.open({
                    animation: true,
                    size : 'lg',
                    component: 'modalTemplateSelect',
                    resolve: {
                        items: function () {
                            return templateService.getTemplatesProjection('emailTemplates');
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    ctrl.detail.contentHtml = selectedItem;
                }, function () {
                    console.log('modal-component dismissed at:');
                });


        }


        function createReplayBodyArchive(body) {
            return "<p>----------------------------</p>"+body;
        };

        function discard() {
            if(isReplayState() || isForwardState()) {
                $state.go('index.email.mailbox.message.detail', {id : ctrl.detail.id});
            }
            if(isComposeState()) {
                $state.go('index.email.mailbox');
            }
        };
        function isReplayState() {
            return $state.includes('index.email.mailbox.message.replay') ? true : false;
        }

        function isForwardState() {
            return $state.includes('index.email.mailbox.message.forward') ? true : false;
        }

        function isComposeState() {
            return $state.includes('index.email.mailbox.message.compose') ? true : false;
        }

        function send() {
            if(isForwardState()) prepareForwardMessage(ctrl.detail);
            if(isReplayState()) prepareReplayMessage(ctrl.detail);
            if(isComposeState()) prepareNewMessage(ctrl.detail);
            prepareAnyMessage(ctrl.detail);

            //var anyVariableInMessage = ctrl.detail.contentHtml.match(/&lt;/);
            var clientVariable = ctrl.detail.contentHtml.match(/%client./);
            var clientCalculation = ctrl.detail.contentHtml.match(/%calculation/);
            var clientContract = ctrl.detail.contentHtml.match(/%contract./);
            var clientPpe = ctrl.detail.contentHtml.match(/.ppes./);

            if (_.has(clientContract, 'index') && _.has(clientCalculation, 'index')) {
                console.log("sdds");
                ctrl.detail.calculation = true;
                ctrl.detail.contract = true;
                 ctrl.openComponentModalSend(ctrl.detail);
            }else if (_.has(clientCalculation, 'index') && !_.has(clientContract, 'index')) {
                ctrl.detail.calculation = true;
                ctrl.detail.contract = false;
                 ctrl.openComponentModalSend(ctrl.detail);
            }
            else if (_.has(clientContract, 'index')  || _.has(clientPpe,'index') && !_.has(clientCalculation, 'index')) {
                ctrl.detail.calculation = false;
                ctrl.detail.contract = true;
                ctrl.openComponentModalSend(ctrl.detail);
            }
            else if(_.has(clientVariable, 'index') && !_.has(clientCalculation,'index') && !_.has(clientContract,'index') ) {
                var mail = {};
                mail = ctrl.detail;
                _.forEach(ctrl.detail.toArray, function(row, index){
                    row.content = ctrl.detail.contentHtml;
                    console.log(row);
                    previewModalService.getContent(row).then(function(data){
                        var detailObject =  _.find(ctrl.detail.toArray,{clientId: row.clientId });
                        mail.toAddress = detailObject.email;
                        mail.contentHtml = data.content;
                        mailboxService.sendMessage_CUSTOM(mail)
                            .then(function(){
                                toaster.pop('success', "Success", "Mail został wysłany");
                                $state.go("index.email.mailbox",{},{reload:'index.email.mailbox'});
                            })
                    });
                })

            }
            else {
                mailboxService.sendMessage_CUSTOM(ctrl.detail)
                    .then(showSuccessMessage)
                    .then(redirectAfterSend);
            }

        }


        function prepareAnyMessage(message) {
            message.contentPlain = $filter('htmlToPlaintext')(message.contentHtml);
            message.mailbox =  getMailboxResource("SENT");
            message.fromAddress = LoggedUserService.getLoggedUser().getActiveEmailAccount().address;
            message.attachments = prepareAttachments(message);

        }

        function prepareAttachments(message) {
            var attachmentsLinksArray = [];
            _.each(message.attachmentsCollection,function (item) {
                attachmentsLinksArray.push(item._links.self.href);
            })
            return attachmentsLinksArray;
        }

        function prepareNewMessage(message) {
            message.created = new Date().toISOString();
            message.toAddress = _.map(message.toArray,'email').join(';');

        }

        function prepareForwardMessage(message) {
            message.toAddress = _.map(message.toArray,'email').join(';');
            removeIdFromMessageDetails(message);
        }

        function prepareReplayMessage(message) {
            message.toAddress = _.map(message.toArray,'email').join(';');
            removeIdFromMessageDetails(message);
        }


        function removeIdFromMessageDetails(message) {
            if(_.has(message,'id')) _.unset(message,'id');
            return message;
        }

        function showSuccessMessage() {
            toaster.pop('success','success','Message Send');
        }

        function redirectAfterSend() {
            $state.go("index.email.mailbox",{},{reload:'index.email.mailbox'});
        }

        function emulatePromiseReq(resolve,reject) {


            sendMessage;
/*            var defer;
            defer = $q.defer();
            $timeout(function () {
                if(!_.isNull(reject) && !_.isUndefined(reject)) return defer.reject(reject);
                if(!_.isNull(resolve)) return defer.resolve(resolve);

            },300);
            return defer.promise;*/

        }


        //------------------------------------ Preview Code ----------------------//

        function openPreview(detail){
            var clientVariable = ctrl.detail.contentHtml.match(/%client./);
            var clientCalculation = ctrl.detail.contentHtml.match(/%calculation/);
            var clientContract = ctrl.detail.contentHtml.match(/%contract./);
            var clientPpe = ctrl.detail.contentHtml.match(/.ppes./);

            if (_.has(clientContract,'index')) {
                detail.contract = true;
                detail.calculation = false;
                if (_.has(clientCalculation, 'index')) {
                    detail.calculation = true;
                }
                openComponentModal(detail);
            }else if (_.has(clientCalculation, 'index')) {
                detail.calculation = true;
                detail.contract = false;
                if (_.has(clientContract, 'index')) {
                    detail.contract = true;
                }
                openComponentModal(detail);
            }
            else {
                openComponentModal(detail)
            }



        }


        function openComponentModal(detail) {
            var modalInstance = $uibModal.open({
                animation: ctrl.animationsEnabled,
                component: 'previewModalComponent',
                resolve: {
                    items: function() {
                        return detail;
                    },
                    send: function(){
                        return false;
                    }
                },
                size:'lg'
            });

            modalInstance.result.then(function(selectedItem) {
                ctrl.selected = selectedItem;
                console.log(selectedItem);

            }, function(data) {
                console.log(data);
            });
        };

        function openComponentModalSend (detail) {
            var modalInstance = $uibModal.open({
                animation: ctrl.animationsEnabled,
                component: 'previewModalComponent',
                resolve: {
                    items: function() {
                        return detail;
                    },
                    send: function(){
                        return true;
                    }
                },
                size:'lg'
            });

            modalInstance.result.then(function(mailToSend) {
                if (mailToSend) {
                    console.log("mailToSend",mailToSend);
                    mailboxService.sendMessage_CUSTOM(mailToSend)
                       .then(showSuccessMessage)
                       .then(redirectAfterSend);
                }else {

                }


            }, function() {});
        };




        //------------------------------------End Preview Code -------------------//
    }
}());
