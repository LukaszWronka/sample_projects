(function () {
    'use strict';

    angular
        .module('sample_project')
        .factory('mailboxService', ['Restangular','$q','$timeout',service]);

    function service(Restangular,$q,$timeout) {

        var service = {
            getMessages : getMessages,
            getMessage : getMessage,
            getUnreadCount : getUnreadCount,
            sendMessage : sendMessage,
            getEmailAccountsByUserId :getEmailAccountsByUserId,
            setEmailAccountToUser : setEmailAccountToUser,
            getMessagesByClientAndMailbox : getMessagesByClientAndMailbox,
            sendMessage_CUSTOM: sendMessage_CUSTOM,
            getClientEmailsList_CUSTOM : getClientEmailsList_CUSTOM,
            generateAttachmentFromAplicationFile : generateAttachmentFromAplicationFile,
            getMsessagePreview:getMsessagePreview

        };



        return service;

        function getUnreadCount(mailboxId) {
            return Restangular
                .all('messages')
                .customGET('search/countByMailboxIdAndIsViewedFalse',{mailboxId: mailboxId});

        }



        function getMessage(id) {
            return  Restangular.one('messages',id).get();

        }


        function sendMessage(message) {
            return Restangular.all('messages').post(message);
        }

        function sendMessage_CUSTOM(message) {
            return Restangular.all('custom').customPOST(message,'messages');
        }

        function getClientEmailsList_CUSTOM(clientId) {
            return Restangular.all('custom').customGET('clients/'+clientId+'/contacts/email/list');
        }

        function generateAttachmentFromAplicationFile(fileId) {
            return Restangular.all('custom').customGET('messages/applicationFiles',{fileId:fileId});
        }

        function getMessages(type,userId) {

            return Restangular
                .all('mailboxes')
                .customGETLIST('search/findOneByMailboxTypeAndEmailAccount_UserId', {userId : userId, type: type})
        }

        function getMsessagePreview(clientId,contractId, ppeId) {
          return $q(function(resolve, reject) {
            setTimeout(function() {
              if (true) {
                resolve( "<p>Content from backeend"+clientId+"</p>");
              } else {
                reject('Greeting  is not allowed.');
              }
            }, 1000);
          });


        }

        function getMessagesByClientAndMailbox(clientId,mailboxType) {

            var endpoint = 'messages';

            return Restangular
                .all('messages')
                .customGET('search/findByClientIdAndMailbox_MailboxType', {clientId : clientId, mailboxType: mailboxType})
                .then(restaangularizeCollection);

            function restaangularizeCollection(response) {
                if(response[endpoint]) {
                    var resource = response[endpoint];
                    var restangularized = Restangular.restangularizeCollection(null, resource, endpoint);
                    return restangularized;
                } else return response;

            }

        }

        function getEmailAccountsByUserId(userId) {

            var endpoint = 'emailAccounts';
            return Restangular
                .all('emailAccounts')
                .customGET('search/findByUserId', {userId : userId})
                .then(restaangularizeCollection);

            function restaangularizeCollection(response) {
                if(response[endpoint]) {
                    var resource = response[endpoint];
                    var restangularized = Restangular.restangularizeCollection(null, resource, endpoint);
                    return restangularized;
                } else return response;

            }
        }

        function setEmailAccountToUser(account) {
            console.log('account',account);
            if(_.has(account,'id')) return account.put();
            else return Restangular.all('emailAccounts/new').post(account);
        }
    }
}());
