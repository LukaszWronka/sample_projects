(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('mailboxMessageDetail', {
            templateUrl: 'app/partials/mailbox/mailbox-message-detail.view.html',
            controller: controller,
            bindings : {
                detail : '<'
            }
        });

    controller.$inject = ['$q', '$scope','$state','$stateParams', '$uibModal', 'toaster','alertService','mailboxService'];
    function controller($q,$scope,$state,$stateParams,$uibModal,toaster,alertService,mailboxService) {
        var ctrl = this;



        console.log('detaleee',ctrl.detail);
        this.$onInit = function () {
            ctrl.detail.attachmentsCollection = [];
            $scope.currentId = $stateParams.id;
            ctrl.detail
                .getList('attachments')
                .then(getApplicationFiles)
                .then(setMessageAsRead)
            // if(!ctrl.detail.isViewed) setMessageAsRead()
        };

        function getApplicationFiles(attachments) {
            _.each(attachments,function (attachment) {
                ctrl.detail.attachmentsCollection.push(attachment.one('applicationFile').get().$object);
            })

        }



        function setMessageAsRead() {
            ctrl.detail.isViewed = true;
            return ctrl.detail.put();
        }





    }
}());
