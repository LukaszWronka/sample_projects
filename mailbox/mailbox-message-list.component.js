(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('mailboxMessageList', {
            templateUrl: 'app/partials/mailbox/mailbox-message-list.view.html',
            bindings: {
                messages : "<",
                type: '<'
            },
            controller: controller
        });

    controller.$inject = ['$q', '$scope', '$uibModal', 'toaster','alertService'];
    function controller($q,$scope,$uibModal,toaster,alertService) {
        var ctrl = this;

        ctrl.getRecipient = getRecipient;
        ctrl.getRecipientColumnName = getRecipientColumnName;

        this.$onInit = function () {
            console.log('$ctrl.displayedMessages',ctrl.messages);

        }

        function getRecipient(message) {

            switch(ctrl.type) {
                case 'RECEIVED' :
                    return message.fromAddress;
                break;

                case 'SENT' :
                    return message.toAddress;
                break;

                case 'DELETED' :
                    return message.fromAddress;
                break;

                default:

                break;
            }
        }

        function getRecipientColumnName() {
            switch(ctrl.type) {
                case 'RECEIVED' :
                    return "FROM"
                    break;

                case 'SENT' :
                    return "TO"
                    break;

                case 'DELETED' :
                    return "FROM/TO"
                    break;

                default:

                    break;
            }
        }
    }
}());
