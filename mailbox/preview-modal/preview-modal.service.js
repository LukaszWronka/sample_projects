(function () {
    'use strict';

    angular
        .module('sample_project')
        .factory('previewModalService', ['Restangular','$q','$timeout',service]);

    function service(Restangular,$q,$timeout) {

        var contracts;
        var ppe;
        var content = Restangular.all('custom/messages/preview');
        var calculation = Restangular.all('calculations');

        var service = {
            setSource : setSource,
            getContracts: getContracts,
            getPpe : getPpe,
            elementToRestangular: elementToRestangular,
            getContent : getContent,
            getCalculation : getCalculation,

        }

        setSource();

        function setSource(){

        }

        function getContracts(id){
            return Restangular.all('clients',id).get('contracts');
        }

        function getPpe(){

        }
        function elementToRestangular(data){
            return Restangular.restangularizeElement(null, data, 'clients');
        }
        function getContent(mail){
            return content.customPOST(mail);
        }
        function getCalculation(id){
            return calculation.customGET('search/findByEvent_ClientId',{'clientId': id});
        }

        return service;
    }

}());
