(function() {
    angular
        .module('sample_project')
        .component('previewModalComponent', {
            templateUrl: 'app/partials/mailbox/preview-modal/preview-modal.view.html',
            controller: ['$scope', 'toaster', 'previewModalService','mailboxService','$state', controller],
            bindings: {
                close: '&',
                resolve: '=',
                dismiss: '&',


            }
        })

    function controller($scope, toaster, previewModalService,mailboxService,$state) {
        var ctrl = this;

        ctrl.selectedContract = selectedContract;
        ctrl.selectedPpe = selectedPpe;
        ctrl.generatePreview = generatePreview;
        ctrl.sentAll = sentAll;

        ctrl.detail = null;
        ctrl.showContracts = false;
        ctrl.showPpes = false;
        ctrl.showContent = false;
        ctrl.showEmailList = false;
        ctrl.countsContracts;
        ctrl.countEmails;
        ctrl.countPpes;
        ctrl.RestanularizedClient;
        ctrl.Client;


        ctrl.contracts = [];
        ctrl.ppe = [];


        ctrl.detail = ctrl.resolve.items;

        //ctrl.countEmails = countSelectedEmail(ctrl.detail);
        ctrl.ok = ok;
        ctrl.cancel = cancel;

// ==============================================================
        ctrl.contractArray =[];
        ctrl.ppesArray  = [];
        ctrl.contentData =[];
        ctrl.calculationArray = [];
        ctrl.canSendMessage = false;






        this.$onInit = function() {
            _.forEach(ctrl.detail.toArray, function(row, index){
                var contractIndex = index;
                ctrl.contentData[index] ={};
                ctrl.contentData[index].clientId = row.clientId;
                var temp = previewModalService.elementToRestangular(row);
                temp.id = temp.clientId;
                temp.getList('contracts').then(function (contract) {
                    ctrl.contractArray[index] = contract;
                })
                previewModalService.getCalculation(temp.id).then(function(calculation){
                    ctrl.calculationArray[index] = calculation.calculations;
                })
            })

            };

        function selectedContract(contract,index){
            ctrl.contentData[index].content =  ctrl.detail.contentHtml;
            ctrl.contentData[index].contractId = contract.id;

            contract.getList('ppes').then(function(ppes){
                ctrl.ppesArray[index] = ppes;
            })
        }
        function selectedCalculation(calculation, index){
            ctrl.contentData[index].calculationId = calculation.id;
        }

        function selectedPpe(ppe,index) {
            ctrl.contentData[index].ppeId = ppe.id;
            ctrl.contentData[index].ppeId = ppe.id;
        }

//===============================================================
        function keepSelectedData(){

        }
        function ok() {
            ctrl.close({
                $value : false
                //$value: ctrl.detail
                //przekazujemy do result po nacisnieciu ok

            });
        };

      function cancel() {
            ctrl.dismiss({
                $value: 'cancel'
            });
        };

        function generatePreview(index){
            ctrl.contentData[index].content = ctrl.detail.contentHtml;
            previewModalService.getContent(ctrl.contentData[index]).then(function (data) {
                console.log(data);
                ctrl.contentHtml = data.content;
                ctrl.showContent = true;
            })
        }

        function sentAll(){
                var mail = {};
                mail = ctrl.detail;
                _.forEach(ctrl.contentData, function(row, index){
                    previewModalService.getContent(row).then(function(data){
                        var detailObject =  _.find(ctrl.detail.toArray,{clientId: row.clientId });
                        mail.toAddress = detailObject.email;
                        mail.contentHtml = data.content;
                        mailboxService.sendMessage_CUSTOM(mail)
                            .then(function(){
                                toaster.pop('success', "Success", "Mail został wysłany");
                                $state.go("index.email.mailbox",{},{reload:'index.email.mailbox'});
                            })
                    });
                })
        }

    }
}());
