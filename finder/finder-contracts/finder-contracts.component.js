/**
 * @TODO
 */

(function() {
    "use strict";

    angular
        .module('sample_project')
        .component('finderContracts', {
            templateUrl: 'app/partials/finder/finder-contracts/finder-contracts.view.html',
            controller: controller,

        });

    function controller($scope, finderService, $filter) {
        var ctrl = this;
        ctrl.test = '';
        ctrl.rowSelectionChange = rowSelectionChange;

        //inicjalizacja
        ctrl.$onInit = function() {
            ctrl.search = '';
            ctrl.dataFromServiceCollectionClients = [];
            ctrl.dataFromServiceCollectionContracts = [];

            ctrl.currentPageClients = 1;
            ctrl.currentPageContracts = 1;
            ctrl.itemsPerPage = 10;
            ctrl.maxSize = 5;

            ctrl.serviceApi = 'contracts',
                ctrl.searchObjectProperties();
        };

        //wywołanie request i stworzenie stringa search
        ctrl.searchObjectProperties = function() {
            var aqFinderContractStatusFrom = '';
            var aqFinderContractStatusTo = '';

            _.forEach($scope.aqFinderFilteredObject, function(value, key) {


                if (!_.isEmpty($scope.aqFinderGlobalSearch)) {
                    console.log('aqFinderGlobalSearch', $scope.aqFinderGlobalSearch);

                    if (key === 'aqFinderContractStatusFrom') {
                        console.log('aqFinderContractStatusFrom');
                        aqFinderContractStatusFrom = 'contractStatus_status>:' + value + '|';

                        ctrl.search = ctrl.search + aqFinderContractStatusFrom;
                    }

                    if (key === 'aqFinderContractStatusTo') {
                        console.log('aqFinderContractStatusTo');
                        aqFinderContractStatusTo = 'contractStatus_status<:' + value + '|';

                        ctrl.search = ctrl.search + aqFinderContractStatusTo;
                    }

                    if ((key !== 'aqFinderContractStatusTo') && (key !== 'aqFinderContractStatusFrom')) {
                        ctrl.search = ctrl.search + key + ':' + value + '|';
                    }

                } else {

                    if (key === 'aqFinderContractStatusFrom') {
                        console.log('aqFinderContractStatusFrom');
                        aqFinderContractStatusFrom = 'contractStatus_status>:' + value + ';';

                        ctrl.search = ctrl.search + aqFinderContractStatusFrom;
                    }

                    if (key === 'aqFinderContractStatusTo') {
                        console.log('aqFinderContractStatusTo');
                        aqFinderContractStatusTo = 'contractStatus_status<:' + value + ';';

                        ctrl.search = ctrl.search + aqFinderContractStatusTo;
                    }

                    if ((key !== 'aqFinderContractStatusTo') && (key !== 'aqFinderContractStatusFrom')) {
                        ctrl.search = ctrl.search + key + ':' + value + ';';
                    }
                };
            });

            var object = finderService
                .getData(
                    ctrl.currentPageClients - 1,
                    ctrl.itemsPerPage,
                    ctrl.serviceApi,
                    ctrl.sortedBy = 'id',
                    ctrl.sortType = 'asc',
                    ctrl.search)
                .then(function(data) {
                    ctrl.dataFromServiceCollectionClients = data;
                    ctrl.totalItemsClients = data.page.totalElements;
                });
        }

        function rowSelectionChange(contract) {
            var index = ctrl.selectedRows.indexOf(contract);
            if (index > -1) {
                ctrl.selectedRows.splice(index, 1);
            } else {
                ctrl.selectedRows.push(contract);
            }
        }

        $scope.aqFinderClearData = function() {
            $scope.aqFinderGlobalSearch = '';
            $scope.aqFinderContractLengthMonths = '';
            $scope.aqFinderContractSignatureMethod = '';
            $scope.aqFinderContractEnergyType = '';
            $scope.aqFinderContractInvoiceType = '';
            $scope.aqFinderContractNumber = '';
            $scope.aqFinderContractStatusFrom = '';
            $scope.aqFinderContractStatusTo = '';
            $scope.aqFinderContractSigned = '';
            $scope.aqFinderFilteredObject = {};
            ctrl.search = '';
            ctrl.currentPageClients = 1;
            ctrl.searchObjectProperties();
        };

        //paginacja
        ctrl.setPage = function(pageNo) {
            ctrl.currentPageClients = pageNo;
        };

        ctrl.pageChanged = function() {
            ctrl.searchObjectProperties();
        };

        $scope.$watch("aqFinderFilteredObject", function(newValue, oldValue) {
            if (newValue !== oldValue) {
                ctrl.search = '';

                ctrl.searchObjectProperties();
            }
        }, true);

        ctrl.aqSearchFilteredArrayNames = [
            'contractLengthMonths',
            'contractSignatureMethod',
            'energyType',
            'invoiceType',

            'number',
            'aqFinderContractStatusFrom',
            'aqFinderContractStatusTo',
            'signed'
        ];

        ctrl.aqSearchFilteredArrayNamesToGlobal = [
            'contractLengthMonths',
            'contractSignatureMethod',
            'energyType',
            'invoiceType',

            'number',
            'aqFinderContractStatusFrom',
            'aqFinderContractStatusTo',
            'signed'
        ];

        $scope.aqSearchCalendarFrom = {
            opened: false
        };
        $scope.aqSearchSelectDateFrom = function() {
            $scope.aqSearchCalendarFrom
                .opened = true;
        }

        $scope.aqFinderGetData = function() {

            $scope.aqFinderFilteredObject = {};

            ctrl.aqSearchFilteredArray = [
                $scope.aqFinderContractLengthMonths,
                $scope.aqFinderContractSignatureMethod,
                $scope.aqFinderContractEnergyType,
                $scope.aqFinderContractInvoiceType,
                $scope.aqFinderContractNumber,
                $scope.aqFinderContractStatusFrom,
                $scope.aqFinderContractStatusTo,
                $scope.aqFinderContractSigned
            ];

            if (!_.isEmpty($scope.aqFinderGlobalSearch)) {
                _.forEach(ctrl.aqSearchFilteredArrayNamesToGlobal, function(value, index) {
                    $scope.aqFinderFilteredObject[ctrl.aqSearchFilteredArrayNamesToGlobal[index]] = $scope.aqFinderGlobalSearch;
                })

            } else {
                _.forEach(ctrl.aqSearchFilteredArray, function(value, index) {

                    if (!_.isEmpty(_.toString(value))) {
                        console.log(value);
                        if (_.isDate(value)) {
                            // value = $filter('date')(value, 'yyyy-MM-dd HH:mm:ss Z');
                            value = $filter('date')(value, "yyyy-MM-ddTHH:mm:ss.000Z");
                            console.log(value);
                        }
                        $scope.aqFinderFilteredObject[ctrl.aqSearchFilteredArrayNames[index]] = value;
                    }
                });
            }
        };
    }
}());
