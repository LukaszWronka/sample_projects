(function() {
    'use strict';
    angular
        .module('sample_project')
        .factory('finderService', ['Restangular', finder]);

    function finder(Restangular) {


        function getData(
            page,
            size,
            serviceApi,
            sortedBy,
            sortType,
            stringWithFilteredData
        ) {

            console.log('service');

            return Restangular
                .one('custom')
                .all(serviceApi)
                .customGET('all', {
                    page: page,
                    size: size,
                    sort: sortedBy + ',' + sortType,
                    search: stringWithFilteredData,
                    projection: 'full'

                }).then(restangularizeCollection);

                function restangularizeCollection(response) {
                    var newResponse= {};

                    newResponse = response[serviceApi] ? Restangular.restangularizeCollection(response, response[serviceApi], serviceApi) : [];
                    newResponse.page = response.page;
                    // console.log('newResponse',newResponse);
                    return newResponse;


                }
        };




        return {
            getData: getData
        };
    }
})();
