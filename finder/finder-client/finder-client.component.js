/**
 * @TODO
 */

(function() {
    "use strict";

    angular
        .module('sample_project')
        .component('finderClients', {
            templateUrl: 'app/partials/finder/finder-client/finder-client.view.html',
            controller: controller,

        });

    function controller($scope, finderService) {
        var ctrl = this;
        ctrl.test = '';
        ctrl.rowSelectionChange = rowSelectionChange;

        //inicjalizacja
        ctrl.$onInit = function() {
            ctrl.search = '';
            $scope.aqFinderGlobalSearch = '';
            ctrl.dataFromServiceCollectionClients = [];
            ctrl.dataFromServiceCollectionContracts = [];

            ctrl.currentPageClients = 1;
            ctrl.currentPageContracts = 1;
            ctrl.itemsPerPage = 10;
            ctrl.maxSize = 5;

            ctrl.serviceApi = 'clients',
                ctrl.searchObjectProperties();
        };


        //wywołanie request i stworzenie stringa search
        ctrl.searchObjectProperties = function() {

            if (!_.isEmpty($scope.aqFinderGlobalSearch)) {
                console.log('Szukanie globalne');
                _.forEach($scope.aqFinderFilteredObject, function(value, key) {
                    ctrl.search = ctrl.search + key + ':' + value + '|';

                });
            } else {
                console.log('szukanie po inputach');
                _.forEach($scope.aqFinderFilteredObject, function(value, key) {
                    ctrl.search = ctrl.search + key + ':' + value + ';';
                });
            }
            console.log(ctrl.search);
            var object = finderService
                .getData(
                    ctrl.currentPageClients - 1,
                    ctrl.itemsPerPage,
                    ctrl.serviceApi,
                    ctrl.sortedBy = 'id',
                    ctrl.sortType = 'asc',
                    ctrl.search)
                .then(function(data) {
                    ctrl.dataFromServiceCollectionClients = data;
                    ctrl.totalItemsClients = data.page.totalElements;
                });
        }

        function rowSelectionChange(client) {
            var index = ctrl.selectedRows.indexOf(client);
            if (index > -1) {
                ctrl.selectedRows.splice(index, 1);
            } else {
                ctrl.selectedRows.push(client);
            }
        }


        $scope.aqFinderClearData = function() {
            $scope.aqFinderGlobalSearch = '';

            $scope.aqFinderClientType = '';
            $scope.aqFinderClientNumber = '';
            $scope.aqFinderClientContractNumber = '';
            $scope.aqFinderClientNip = '';
            $scope.aqFinderClientKrs = '';
            $scope.aqFinderClientName = '';
            $scope.aqFinderClientFirstName = '';
            $scope.aqFinderClientSurname = '';
            $scope.aqFinderClientPesel = '';
            $scope.aqFinderClientMail = '';
            $scope.aqFinderClientPhoneNumber = '';
            $scope.aqFinderClientCity = '';
            $scope.aqFinderClientStreet = '';
            $scope.aqFinderClientBuildingNumber = '';
            $scope.aqFinderClientLocalNumber = '';
            $scope.aqFinderClientPostalCode = '';
            $scope.aqFinderFilteredObject = {};
            ctrl.search = '';
            ctrl.currentPageClients = 1;
            ctrl.searchObjectProperties();
        };


        //paginacja
        ctrl.setPage = function(pageNo) {
            ctrl.currentPageClients = pageNo;
        };

        ctrl.pageChanged = function() {
            ctrl.searchObjectProperties();
        };

        $scope.$watch("aqFinderFilteredObject", function(newValue, oldValue) {
            if (newValue !== oldValue) {
                ctrl.search = '';

                ctrl.searchObjectProperties();

            }
        }, true);


        ctrl.aqSearchFilteredArrayNames = [
            'type',
            'number',
            'contracts_number',
            'nip',
            'krs',

            'companyName',
            'firstName',
            'surname',
            'pesel',
            'emailContacts_emailAddress',
            'phoneContacts_phoneNumber',

            'addresses_city',
            'addresses_street',
            'addresses_house',
            'addresses_apartment',
            'addresses_postalCode',
        ];

        ctrl.aqSearchFilteredArrayNamesToGlobal = [
            'type',
            'number',
            'contracts_number',
            'nip',
            'krs',

            'companyName',
            'firstName',
            'surname',
            'pesel',
            'emailContacts_emailAddress',
            'phoneContacts_phoneNumber',

            'addresses_city',
            'addresses_street',
            'addresses_house',
            'addresses_apartment',
            'addresses_postalCode',
        ];

        $scope.aqFinderGetData = function() {
            $scope.aqFinderFilteredObject = {};

            ctrl.aqSearchFilteredArray = [
                $scope.aqFinderClientType,
                $scope.aqFinderClientNumber,
                $scope.aqFinderClientContractNumber,
                $scope.aqFinderClientNip,
                $scope.aqFinderClientKrs,
                $scope.aqFinderClientName,
                $scope.aqFinderClientFirstName,
                $scope.aqFinderClientSurname,
                $scope.aqFinderClientPesel,
                $scope.aqFinderClientMail,
                $scope.aqFinderClientPhoneNumber,
                $scope.aqFinderClientCity,
                $scope.aqFinderClientStreet,
                $scope.aqFinderClientBuildingNumber,
                $scope.aqFinderClientLocalNumber,
                $scope.aqFinderClientPostalCode
            ];


            if (!_.isEmpty($scope.aqFinderGlobalSearch)) {
                _.forEach(ctrl.aqSearchFilteredArrayNamesToGlobal, function(value, index) {
                    $scope.aqFinderFilteredObject[ctrl.aqSearchFilteredArrayNamesToGlobal[index]] = $scope.aqFinderGlobalSearch;
                })

            } else {
                _.forEach(ctrl.aqSearchFilteredArray, function(value, index) {

                    if (!_.isEmpty(_.toString(value))) {
                        // if (_.isDate(value)) {
                        //     value = $filter('date')(value, "yyyy-MM-dd'T'HH:mm:ss");
                        // }
                        $scope.aqFinderFilteredObject[ctrl.aqSearchFilteredArrayNames[index]] = value;
                    }
                })
            }


        };


    }

}());
