/**
 * @TODO
 */

(function() {
    "use strict";

    angular
        .module('sample_project')
        .component('finderPpes', {
            templateUrl: 'app/partials/finder/finder-ppes/finder-ppes.view.html',
            controller: controller,

        });

    function controller($scope, finderService) {
        var ctrl = this;
        ctrl.test = '';
        ctrl.rowSelectionChange = rowSelectionChange;

        //inicjalizacja
        ctrl.$onInit = function() {
            ctrl.search = '';
            ctrl.dataFromServiceCollectionPpe = [];
            ctrl.dataFromServiceCollectionContracts = [];

            ctrl.currentPageClients = 1;
            ctrl.currentPageContracts = 1;
            ctrl.itemsPerPage = 10;
            ctrl.maxSize = 5;

            ctrl.serviceApi = 'ppes',
                ctrl.searchObjectProperties();
        };


        //wywołanie request i stworzenie stringa search
        ctrl.searchObjectProperties = function() {

            if (!_.isEmpty($scope.aqFinderGlobalSearch)) {
                _.forEach($scope.aqFinderFilteredObject, function(value, key) {
                    ctrl.search = ctrl.search + key + ':' + value + '|';
                });

            } else {
                _.forEach($scope.aqFinderFilteredObject, function(value, key) {
                    ctrl.search = ctrl.search + key + ':' + value + ';';
                });
            };
            console.log(ctrl.search);
            var object = finderService
                .getData(
                    ctrl.currentPageClients - 1,
                    ctrl.itemsPerPage,
                    ctrl.serviceApi,
                    ctrl.sortedBy = 'id',
                    ctrl.sortType = 'asc',
                    ctrl.search)
                .then(function(data) {
                    ctrl.dataFromServiceCollectionPpe = data;
                    ctrl.totalItemsClients = data.page.totalElements;
                });
        }

        function rowSelectionChange(ppes) {
            var index = ctrl.selectedRows.indexOf(ppes);
            if (index > -1) {
                ctrl.selectedRows.splice(ppes, 1);
            } else {
                ctrl.selectedRows.push(ppes);
            }
        }

        $scope.aqFinderClearData = function() {
            $scope.aqFinderPpeNumber = '';
            $scope.aqFinderPpeMeterNumber = '';
            $scope.aqFinderPpeRegistrationNumber = '';
            $scope.aqFinderPpePower = '';
            $scope.aqFinderPpeYearlyUsage = '';
            $scope.aqFinderPpeCity = '';
            $scope.aqFinderPpeStreet = '';
            $scope.aqFinderPpeHouse = '';
            $scope.aqFinderPpeApartment = '';
            $scope.aqFinderPpePostalCode = '';
            $scope.aqFinderPpeTariff = '';
            $scope.aqFinderFilteredObject = {};
            ctrl.search = '';
            ctrl.currentPageClients = 1;
            ctrl.searchObjectProperties();
        };



        //paginacja
        ctrl.setPage = function(pageNo) {
            ctrl.currentPageClients = pageNo;
        };

        ctrl.pageChanged = function() {
            ctrl.searchObjectProperties();
        };

        $scope.$watch("aqFinderFilteredObject", function(newValue, oldValue) {
            if (newValue !== oldValue) {
                ctrl.search = '';

                ctrl.searchObjectProperties();

            }
        }, true);

        ctrl.aqSearchFilteredArrayNames = [
            'number',
            'meterNumber',
            'registrationNumber',
            'power',
            'yearlyUsage',
            'city',
            'street',
            'house',
            'apartment',
            'postalCode',
            'tariff'
        ];

        ctrl.aqSearchFilteredArrayNamesToGlobal = [
            'number',
            'meterNumber',
            'registrationNumber',
            'power',
            'yearlyUsage',
            'city',
            'street',
            'house',
            'apartment',
            'postalCode',
            // 'tariff'
        ];

        $scope.aqFinderGetData = function() {
            $scope.aqFinderFilteredObject = {};

            ctrl.aqSearchFilteredArray = [
                $scope.aqFinderPpeNumber,
                $scope.aqFinderPpeMeterNumber,
                $scope.aqFinderPpeRegistrationNumber,
                $scope.aqFinderPpePower,
                $scope.aqFinderPpeYearlyUsage,
                $scope.aqFinderPpeCity,
                $scope.aqFinderPpeStreet,
                $scope.aqFinderPpeHouse,
                $scope.aqFinderPpeApartment,
                $scope.aqFinderPpePostalCode,
                $scope.aqFinderPpeTariff
            ];
            //
            // _.forEach(ctrl.aqSearchFilteredArray, function(value, index) {
            //
            //     if (!_.isEmpty(_.toString(value))) {
            //         // if (_.isDate(value)) {
            //         //     value = $filter('date')(value, "yyyy-MM-dd'T'HH:mm:ss");
            //         // }
            //         $scope.aqFinderFilteredObject[ctrl.aqSearchFilteredArrayNames[index]] = value;
            //     }
            // });


            if (!_.isEmpty($scope.aqFinderGlobalSearch)) {

                _.forEach(ctrl.aqSearchFilteredArrayNamesToGlobal, function(value, index) {
                    $scope.aqFinderFilteredObject[ctrl.aqSearchFilteredArrayNamesToGlobal[index]] = _.toString($scope.aqFinderGlobalSearch);
                })

            } else {
                _.forEach(ctrl.aqSearchFilteredArray, function(value, index) {

                    if (!_.isEmpty(_.toString(value))) {
                        // if (_.isDate(value)) {
                        //     value = $filter('date')(value, "yyyy-MM-dd'T'HH:mm:ss");
                        // }
                        $scope.aqFinderFilteredObject[ctrl.aqSearchFilteredArrayNames[index]] = value;
                    }
                });
            }

        };


    }

}());
