/**
 * @TODO
 */

(function() {
    "use strict";

    angular
        .module('sample_project')
        .component('finder', {
            templateUrl: 'app/partials/finder/finder.view.html',
            controller: controller,

        });

    function controller($scope, finderService) {
        var ctrl = this;
        ctrl.test = '';
        //inicjalizacja
        ctrl.$onInit = function() {
            ctrl.search = '';
            ctrl.dataFromServiceCollectionClients = [];
            ctrl.dataFromServiceCollectionContracts = [];

            ctrl.currentPageClients = 1;
            ctrl.currentPageContracts = 1;
            ctrl.itemsPerPage = 10;
            ctrl.maxSize = 5;

            ctrl.serviceApi = 'clients',
                ctrl.searchObjectProperties();
        };


        //wywołanie request i stworzenie stringa search
        ctrl.searchObjectProperties = function() {

            _.forEach($scope.aqFinderFilteredObject, function(value, key) {
                ctrl.search = ctrl.search + key + ':' + value + ';';
                console.log(ctrl.search);
            });

            var object = finderService
                .getData(
                    ctrl.currentPageClients - 1,
                    ctrl.itemsPerPage,
                    ctrl.serviceApi,
                    ctrl.sortedBy = 'id',
                    ctrl.sortType = 'asc',
                    ctrl.search)
                .then(function(data) {
                    ctrl.dataFromServiceCollectionClients = data;
                    ctrl.totalItemsClients = data.page.totalElements;
                });
        }

        //paginacja
        ctrl.setPage = function(pageNo) {
            ctrl.currentPageClients = pageNo;
        };

        ctrl.pageChanged = function() {
            ctrl.searchObjectProperties();
        };

        $scope.$watch("aqFinderFilteredObject", function(newValue, oldValue) {
            if (newValue !== oldValue) {
                ctrl.search = '';

                ctrl.searchObjectProperties();

            }
        }, true);

        ctrl.aqSearchFilteredArrayNames = [
            'type',
            'number',
            'contracts_number',
            'nip',
            'krs',

            'companyName',
            'firstName',
            'surname',
            'pesel',
            'emailContacts_emailAddress',
            'phoneContacts_phoneNumber',

            'addresses_city',
            'addresses_street',
            'addresses_house',
            'addresses_apartment',
            'addresses_postalCode',
        ];

        $scope.aqFinderGetData = function() {
            $scope.aqFinderFilteredObject = {};

            ctrl.aqSearchFilteredArray = [
                $scope.aqFinderClientType,
                $scope.aqFinderClientNumber,
                $scope.aqFinderClientContractNumber,
                $scope.aqFinderClientNip,
                $scope.aqFinderClientKrs,
                $scope.aqFinderClientName,
                $scope.aqFinderClientFirstName,
                $scope.aqFinderClientSurname,
                $scope.aqFinderClientPesel,
                $scope.aqFinderClientMail,
                $scope.aqFinderClientPhoneNumber,
                $scope.aqFinderClientCity,
                $scope.aqFinderClientStreet,
                $scope.aqFinderClientBuildingNumber,
                $scope.aqFinderClientLocalNumber,
                $scope.aqFinderClientPostalCode
            ];

            _.forEach(ctrl.aqSearchFilteredArray, function(value, index) {

                if (!_.isEmpty(_.toString(value))) {
                    // if (_.isDate(value)) {
                    //     value = $filter('date')(value, "yyyy-MM-dd'T'HH:mm:ss");
                    // }
                    $scope.aqFinderFilteredObject[ctrl.aqSearchFilteredArrayNames[index]] = value;
                }
            });
        };


    }

}());
