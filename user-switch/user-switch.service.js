(function () {

    'use strict';

    angular
        .module('sample_project')
        .factory('userSwitchService', ['Restangular','$q','$timeout',controller]);

    function controller(Restangular, $q, $timeout) {

        var service = {
            getUser : getUser,
        };

        return service;

        function getUser(id) {
            return Restangular.all('users').get(id);
        }


    }

}());
