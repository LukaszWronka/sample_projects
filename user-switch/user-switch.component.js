(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('userSwitch', {
            templateUrl: 'app/partials/user-switch/user-switch.view.html',
            controller: ['LoggedUserService', 'userSwitchService','$cookies','$state',controller],
        });

    function controller(LoggedUserService, userSwitchService,$cookies,$state) {
        var ctrl = this;

        ctrl.users = [];
        ctrl.activeUserId = null;

        ctrl.switchUser = switchUser;

        this.$onInit = function () {
            setMyselfUser();
            setSubstitutionUsers();
            getActiveUser();
        };


        function setMyselfUser() {
            ctrl.users.push(LoggedUserService.getLoggedUser());
        }

        function setSubstitutionUsers() {
            _.each(LoggedUserService.getSubstitutions(), setUser);
            function setUser(id) {
                userSwitchService.getUser(id).then(function (user) {
                    ctrl.users.push(user);
                });
            }
        }

        function switchUser() {
            var selectedUser = _.find(ctrl.users,{id: ctrl.activeUserId});
            LoggedUserService.setActiveUser(selectedUser);
            $state.reload();
        }

        function getActiveUser() {
            ctrl.activeUserId = LoggedUserService.getLoggedUser().activeUser.id;
        }


    }


}());
