(function() {
    angular
        .module('sample_project')
        .directive('aqSearch', search);

    function search() {

        var controller = ['$scope', '$filter', function($scope, $filter) {

            var ctrl = this;

            $scope.aqSearchFilteredObject = {};

            ctrl.aqSearchFilteredArrayNames = [
                'dateFrom',
                'dateTo',
                'emailAddress',
                'phoneNumber',
                'statusFrom',
                'statusTo',
                'aqSearchSource',
                'aqSearchMedium',
                'aqSearchCampaign',

                'firstName'
            ];

            $scope.aqSearchGetData = function() {

                $scope.aqSearchFilteredObject = {};

                ctrl.aqSearchFilteredArray = [
                    $scope.aqSearchDateFrom,
                    $scope.aqSearchDateTo,
                    $scope.aqSearchEmail,
                    $scope.aqSearchPhone,

                    $scope.aqSearchStatusFrom,
                    $scope.aqSearchStatusTo,

                    $scope.aqSearchSource,
                    $scope.aqSearchMedium,
                    $scope.aqSearchCampaign,

                    $scope.aqSearchFirstName
                ];

                _.forEach(ctrl.aqSearchFilteredArray, function(value, index) {

                    if (!_.isEmpty(_.toString(value))) {
                        if (_.isDate(value)) {
                            value = $filter('date')(value, "yyyy-MM-dd'T'HH:mm:ss");
                        }
                        $scope.aqSearchFilteredObject[ctrl.aqSearchFilteredArrayNames[index]] = value;
                    }
                });
            };

            $scope.aqSearchCalendarFrom = {
                opened: false
            };
            $scope.aqSearchSelectDateFrom = function() {
                $scope.aqSearchCalendarFrom
                    .opened = true;
            }

            $scope.aqSearchCalendarTo = {
                opened: false
            };
            $scope.aqSearchSelectDateTo = function() {
                $scope.aqSearchCalendarTo
                    .opened = true;
            }
        }];

        return {
            restrict: 'AE',
            templateUrl: "app/components/sample_project-table/search/aq-search.view.html",
            require: '^aqTable',

            scope: {
                aqSearchDateFormat: "@",
                aqSearchFilteredObject: '='
            },
            controller: controller,
        };
    }
}());
