(function() {
    'use strict';
    angular
        .module('sample_project')
        .factory('sample_project', ['Restangular', service]);

    function service(Restangular) {

        var defaultQueryParam = {
            currentPage : 0,
            sortType : 'desc',
            itemPerPage : 10,
            sortedBy : 'id'
        }

        function getData(
            page,
            size,
            serviceApi,
            sortedBy,
            sortType,
            stringWithFilteredData
        ) {

            page = page || defaultQueryParam.currentPage;
            size = size || defaultQueryParam.itemPerPage;
            sortType = sortType || defaultQueryParam.sortType;
            sortedBy = sortedBy || defaultQueryParam.sortedBy;
            stringWithFilteredData = stringWithFilteredData || '';

            return Restangular
                .one('custom')
                .all(serviceApi)
                .customGET('all', {
                    page: page,
                    size: size,
                    sort: sortedBy + ',' + sortType,

                    search: stringWithFilteredData,
                    projection:'full'
                })
                .then(restangularizeCollection)

            function restangularizeCollection(response) {
                var newResponse= {};

                newResponse = response[serviceApi] ? Restangular.restangularizeCollection(response, response[serviceApi], serviceApi) : [];
                newResponse.page = response.page;
                console.log('newResponse',newResponse);
                return newResponse;


            }
        }


        return {
            getData: getData
        };
    }
})();
