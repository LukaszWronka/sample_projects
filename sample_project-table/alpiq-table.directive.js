(function() {
    angular
        .module('sample_project')
        .directive('aqTable', table);

    function table() {

        var controller = ['$scope', 'sample_project', function($scope, sample_project) {

            var ctrl = this;

            ctrl.aqTableFilteredSearchString = '';

            $scope.$watch("aqTableFiltered", function(newValue, oldValue) {
                if (newValue !== oldValue) {
                    $scope.aqTableSelectedRows = [];

                    $scope.aqTableFiltered = newValue;
                    ctrl.aqTableFilteredSearchString = ctrl.aqTableSearchValue($scope.aqTableFiltered)
                    ctrl.aqTableGetData();
                }
            }, true);


            $scope.$watch("aqTableSort", function(newValue, oldValue) {
                if (newValue !== oldValue) {
                    $scope.aqTableSelectedRows = [];
                    console.log('new sort');
                    $scope.aqTableSortType = newValue.sortType;
                    $scope.aqTableSortedBy = newValue.sortedBy;
                    ctrl.aqTableGetData();
                }
            });

            $scope.$watch("aqTableCurrentPage", function(newValue, oldValue) {
                if (newValue !== oldValue) {
                    console.log('new page');
                    $scope.aqTableCurrentPage = newValue;
                    ctrl.aqTableGetData();
                }
            });

            ctrl.$onInit = function() {
                $scope.aqTableCurrentPage = 1;
                $scope.aqTableSortType = "asc";
                $scope.aqTableSortedBy = "statusDate";
                //ctrl.aqTableGetData();
            };


            ctrl.aqTableGetData = function() {

                sample_project
                    .getData(
                        $scope.aqTableCurrentPage - 1,
                        $scope.aqTableItemsPerPage,
                        $scope.aqTableGetDataFromService,

                        $scope.aqTableSortedBy,
                        $scope.aqTableSortType,
                        ctrl.aqTableFilteredSearchString


                    )
                    .then(function(data) {
                        $scope.aqTableTotalItems = data.page.totalElements;

                        $scope.aqTableEventCollection = data;

                    });
            };

            ctrl.aqTableSearchValue = function(aqTableFiltered) {
                var array = [];
                var search = '';
                var firstName = '';

                var statusDate = 'statusDate';
                var dateFrom = '';
                var dateTo = '';
                var phoneContacts = '';
                var emailContacts = '';
                var statusFrom = '';
                var statusTo = '';

                _.forEach(aqTableFiltered, function(value, key) {

                    if ($scope.aqTableGetDataFromService === 'clients') {
                        if (key === 'firstName') {
                            var array = value.split(',');
                            if (array.length >= 2) {
                                firstName = 'firstName()';
                                firstName = firstName + array.toString() + ';';
                            } else {
                                firstName = 'firstName:' + value + ';';
                            }
                        }
                        if (key === 'dateFrom') {
                            dateFrom = 'statusDate>:' + value + ';';
                        }
                        if (key === 'dateTo') {
                            dateTo = 'statusDate<' + value + ';';
                        }
                        if (key === 'phoneNumber') {
                            phoneContacts = 'phoneContacts_phoneNumber:' + value + ';';
                        }
                        if (key === 'emailAddress') {
                            emailContacts = 'emailContacts_emailAddress:' + value + ';';
                        }

                        // if (key === 'name') {
                        //     clientStatuses = 'clientStatuses_name:' + value + ';';
                        // }

                        if (key === 'statusFrom') {
                            statusFrom = 'clientStatus_status>:' + value + ';';
                        }
                        if (key === 'statusTo') {
                            statusTo = 'clientStatus_status<' + value + ';';
                        }


                    }

                    if ($scope.aqTableGetDataFromService === 'events') {

                        if (key === 'firstName') {
                            var array = value.split(',');
                            if (array.length >= 2) {
                                firstName = 'firstName()';
                                firstName = firstName + array.toString() + ';';
                            } else {
                                firstName = 'firstName:' + value + ';';
                            }
                        }
                        if (key === 'dateFrom') {
                            dateFrom = 'statusDate>:' + value + ';';
                        }
                        if (key === 'dateTo') {
                            dateTo = 'statusDate<' + value + ';';
                        }
                        if (key === 'phoneNumber') {
                            phoneContacts = 'phone:' + value + ';';
                        }
                        if (key === 'emailAddress') {
                            emailContacts = 'email:' + value + ';';
                        }
                        // if (key === 'name') {
                        //     eventStatus = 'eventStatus_name:' + value + ';';
                        // }

                        if (key === 'statusFrom') {
                            statusFrom = 'eventStatus_status>:' + value + ';';
                        }
                        if (key === 'statusTo') {
                            statusTo = 'eventStatus_status<' + value + ';';
                        }

                    }

                });
                console.log('$scope.aqTableDefaultDataQueryParam',$scope.aqTableDefaultDataQueryParam);
                console.log(typeof $scope.aqTableDefaultDataQueryParam);
                search = firstName + dateFrom + dateTo + phoneContacts + emailContacts + statusFrom + statusTo;

                if($scope.aqTableDefaultDataQueryParam) {
                    search+=$scope.aqTableDefaultDataQueryParam;
                }
                return search;
            };





        }];

        return {
            restrict: 'A',
            scope: {
                aqTableCurrentPage: '=',
                aqTableItemsPerPage: '=',
                aqTableGetDataFromService: '@',
                aqTableEventCollection: '=',
                aqTableTotalItems: '=',
                aqTableSort: '=',
                aqTableFiltered: '=',
                aqTableDefaultDataQueryParam: "<",
                aqTableSelectedRows: "="
            },
            controller: controller,
        };
    }
}());
