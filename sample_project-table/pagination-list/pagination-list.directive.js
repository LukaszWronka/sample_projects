(function() {
    angular
        .module('sample_project')
        .directive('aqPagination', pagination);

    function pagination() {

        var controller = ['$scope', function($scope) {

            var ctrl = this;

            $scope.aqPagMaxSize = 4;
            ctrl.$onInit = function() {
            };

            $scope.setPage = function(pageNo) {
                $scope.aqPagCurrentPage = pageNo;
            };

            $scope.pageChanged = function() {

            };
        }];

        return {
            restrict: 'AE',
            templateUrl: "app/components/sample_project-table/pagination-list/pagination-list.view.html",
            require: '^aqTable',
            scope: {
                aqPagCurrentPage:'=',
                aqPagItemsPerPage:'=',
                aqPagTotalItems:'=',
            },
            controller: controller,
        };
    }
}());
