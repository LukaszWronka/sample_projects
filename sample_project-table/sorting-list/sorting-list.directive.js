(function() {
    angular
        .module('sample_project')
        .directive('aqList', pagination);

    function pagination() {

        var controller = ['$scope', function($scope) {

            var ctrl = this;
            var status = true;
            $scope.aqListsortType = 'asc';
            $scope.aqListAscDescIcon = "glyphicon glyphicon-sort-by-attributes";
            $scope.test= false;


            $scope.aqListChangeSort = function() {
                // console.log($scope.test);
                // console.log($('[ng-model="test"]'));
                //
                // $('[ng-model="test"]').removeClass("glyphicon glyphicon-sort-by-attributes");
                // $(this).addClass("glyphicon-sort-by-attributes");

                status = !status;
                $scope.aqListAscDescIcon='';
                status ? aqListsortAsc() : aqListsortDesc();
            }

            function aqListsortAsc() {
                $scope.aqListAscDescIcon = 'glyphicon glyphicon-sort-by-attributes';
                $scope.aqListSort = {
                    sortType: 'asc',
                    sortedBy: $scope.aqListSortedBy
                };
            }

            function aqListsortDesc() {
                $scope.aqListAscDescIcon = 'glyphicon glyphicon-sort-by-attributes-alt';
                $scope.aqListSort = {
                    sortType: 'desc',

                    sortedBy: $scope.aqListSortedBy
                };
            }

        }];

        return {
            restrict: 'AE',
            templateUrl: "app/components/sample_project-table/sorting-list/sorting-list.view.html",
            require: '^aqTable',
            transclude: true,
            scope: {
                aqListSort: '=',
                aqListSortedBy: '@'
            },
            controller: controller,
        };
    }
}());
