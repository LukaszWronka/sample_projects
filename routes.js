/* global angular, _ */
(function () {
    'use strict';
    function config($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/index/dashboard");
        $stateProvider
            .state('start', {
                url: "/",
                templateUrl: "app/partials/dashboard.html"
            })
            .state('index', {
                abstract: true,
                url: "/index",
                templateUrl: "app/partials/common/views/default-content.html",
                resolve: {
                    LoggedUserService: 'LoggedUserService',
                    loggedUser: ['LoggedUserService', '$stateParams', function (LoggedUserService) {
                        return LoggedUserService.setLoggedUser();
                    }],
                    otherUsersAccessList: ['loggedUser', function (loggedUser) {
                        return loggedUser.getAccessListOfOtherUsers();
                    }]
                }
            })
            .state('index.logout', {
                    url: "/logout/:message",
                    templateUrl: "app/partials/common/views/logout.html",
                    controller : ['$state','$stateParams','$window',function($state,$stateParams, $window) {
                        $window.location.href = 'logout?message='+$stateParams.message;
                    }]

                }
            )

            .state('index.tasks', {
                url: "/tasks",
                template: "<tasks task-collection = '$resolve.taskCollection' user-role = '$resolve.userRole'></tasks>",
                ncyBreadcrumb: {
                    label: 'Tasks'
                },
                resolve: {
                    userRole : ['loggedUser', function(loggedUser){
                        return loggedUser.isAdmin();
                    }],

                    taskCollection : ['taskService','userRole','loggedUser', function (taskService,userRole,loggedUser) {
                        return (loggedUser.isAdmin() || loggedUser.isManager()) ? taskService.get() : taskService.getTaskByUserId(loggedUser.getActiveUser().id);


                    }],
                }
            })
            .state('index.tasks.add', {
                url: "/add",
                views: {
                    "details": {
                        template: "<tasks-add user-role = $resolve.userRole></tasks-add>"
                    }
                },
                ncyBreadcrumb: {
                    label: 'add'
                },
                resolve : {
                    userRole : ['loggedUser', function(loggedUser){
                        return loggedUser.isAdmin();
                    }],
                }

            })
            .state('index.tasks.details', {
                url: "/details/:taskId",
                views: {
                    "details": {
                        template: "<tasks-details detailed-task = '$resolve.detailedTask' task-config-collection = '$resolve.taskConfigCollection'></tasks-details>"
                    }
                },
                ncyBreadcrumb: {
                    label: 'details'
                },
                resolve : {
                    detailedTask : ['taskService','$stateParams', function (taskService,$stateParams) {
                        return taskService.get($stateParams.taskId);
                    }],
                    taskConfigCollection : ['taskService','$stateParams','detailedTask', function (taskService,$stateParams,detailedTask) {
                        return taskService.getTaskConfing(detailedTask);
                    }],
                }

            })
            .state('index.tasks.edit', {
                url: "/edit/:taskId",
                views: {
                    "details": {
                        template: "<tasks-edit detailed-task = '$resolve.detailedTask' task-config-collection = '$resolve.taskConfigCollection'></tasks-edit>"
                    }
                },
                ncyBreadcrumb: {
                    label: 'edit'
                },
                resolve : {
                    detailedTask : ['taskService','$stateParams', function (taskService,$stateParams) {
                        return taskService.get($stateParams.taskId);
                    }],
                    taskConfigCollection : ['taskService','$stateParams','detailedTask', function (taskService,$stateParams,detailedTask) {
                        return taskService.getTaskConfing(detailedTask);
                    }],
                }
            })


            .state('index.email.mailbox', {
                url: "/mailbox",
                templateUrl: "app/partials/mailbox/mailbox.view.html",
                ncyBreadcrumb: {
                    label: "Mailbox"
                },
                resolve: {
                    mailboxService: 'mailboxService',
                    unread: ['mailboxService', function (mailboxService) {
                        return mailboxService.getUnreadCount();
                    }],
                    addressBookCrudService: 'addressBookCrudService',
                    addressBook: ['addressBookCrudService', function (addressBookCrudService) {
                        return addressBookCrudService.getAddressBookWithEntries();
                    }],
                    LoggedUserService: 'LoggedUserService',
                    emailAccountsMailboxes: ['loggedUser', function (loggedUser) {
                        return loggedUser.getMailboxesForAccounts();
                    }],
                    emailMessagesWatcher: ['loggedUser', function (loggedUser) {

                    }]
                },
                controller: ['$scope', 'addressBook', 'emailAccountsMailboxes', 'loggedUser', function ($scope, addressBook, emailAccountsMailboxes, loggedUser) {
                    $scope.emailAccountsMailboxes = emailAccountsMailboxes;
                    $scope.addressBook = addressBook;
                }]
            })
            .state('index.email.mailbox.box', {
                url: "/:emailAccountId/:boxType",
                ncyBreadcrumb: {
                    label: '{{$resolve.activeEmailAccount.address}} - {{$resolve.selectedMailboxType}}'
                },
                views: {
                    'content': {
                        template: "<mailbox-message-list type='$resolve.selectedMailboxType' selected-mailbox='$resolve.selectedMailbox' messages='$resolve.messages'></mailbox-message-list>"
                    }
                },
                resolve: {
                    mailboxService: 'mailboxService',
                    messages: ['$stateParams', 'emailAccountsMailboxes', function ($stateParams, emailAccountsMailboxes) {
                        var selectedEmailAccount = _.find(emailAccountsMailboxes, {id: parseInt($stateParams.emailAccountId)});
                        var selectedMailbox = _.find(selectedEmailAccount.mailboxes, {mailboxType: $stateParams.boxType});
                        console.log('selectedMailbox', selectedMailbox);
                        return selectedMailbox.getList('messages');
                    }],
                    selectedMailboxType: ['$stateParams', function ($stateParams) {
                        return $stateParams.boxType;
                    }],
                    activeEmailAccount: ['loggedUser', '$stateParams', function (loggedUser, $stateParams) {
                        loggedUser.setActiveEmailAccount(parseInt($stateParams.emailAccountId));
                        //   loggedUser.setActiveMailboxByType($stateParams.boxType);
                        return loggedUser.getActiveEmailAccount();
                    }],
                    activeMailbox: ['loggedUser', '$stateParams', function (loggedUser, $stateParams) {
                        console.log(loggedUser);
                        return true;
                    }]
                }
            .state('index.email.mailbox.message', {
                url: "/message/{id:int}",
                abstract: true,
                views: {
                    'content@index.email.mailbox': {
                        template: "<ui-view/>"
                    }
                },
                ncyBreadcrumb: {
                    label: 'message'
                },
                resolve: {
                    mailboxService: 'mailboxService',
                    tinymceConfigurationService: 'tinymceConfigurationService',
                    detail: ['mailboxService', '$stateParams', function (mailboxService, $stateParams) {
                        if ($stateParams.id)
                            return mailboxService.getMessage($stateParams.id);
                    }],
                    activeEmailAccount: ['loggedUser', function (loggedUser) {
                        console.log(loggedUser.getActiveEmailAccount());
                        return loggedUser.getActiveEmailAccount();
                    }],
                    clientService: 'clients',
                    userClients: ['clientService', 'mailboxService', function (clientService, mailboxService) {
                        return clientService.get();
                    }]

                }
            })
            .state('index.email.mailbox.message.detail', {
                url: "/detail",
                ncyBreadcrumb: {
                    label: 'detail'
                },
                template: "<mailbox-message-detail user-clients='$resolve.userClients' detail='$resolve.detail'></mailbox-message-detail>"
            })
            .state('index.email.mailbox.message.forward', {
                url: "/forward",
                ncyBreadcrumb: {
                    label: 'forward'
                },
                template: "<mailbox-message-compose user-clients='$resolve.userClients' detail='$resolve.detail' address-book='$resolve.addressBook'></mailbox-message-compose>"
            })
            .state('index.email.mailbox.message.replay', {
                url: "/replay",
                ncyBreadcrumb: {
                    label: 'replay'
                },
                template: "<mailbox-message-compose user-clients='$resolve.userClients' detail='$resolve.detail' address-book='$resolve.addressBook'></mailbox-message-compose>"
            })
            .state('index.email.mailbox.message.compose', {
                url: "/compose",
                ncyBreadcrumb: {
                    label: 'compose'
                },
                template: "<mailbox-message-compose user-clients='$resolve.userClients' address-book='$resolve.addressBook'></mailbox-message-compose>"
            })
            .state('index.email.addressBook', {
                url: "/address-book",
                ncyBreadcrumb: {
                    label: 'Address Book'
                },
                views: {
                    'content': {
                        template: "<div class='aq-box'><address-book-crud></address-book-crud></div>"
                    }
                }
            })
            .state('index.finder', {
                url: "/finder",
                template: "<finder></finder>",
                ncyBreadcrumb: {
                    label: 'Finder'
                },

            })
            .state('index.finder.clients', {
                url: "/clients",
                template: "<finder-clients></finder-clients>",
                ncyBreadcrumb: {
                    label: 'Clients'
                },

            })
            .state('index.finder.contracts', {
                url: "/contracts",
                template: "<finder-contracts></finder-contracts>",
                ncyBreadcrumb: {
                    label: 'Contracts'
                },

            })
            .state('index.finder.ppes', {
                url: "/ppes",
                template: "<finder-ppes></finder-ppes>",
                ncyBreadcrumb: {
                    label: 'PPES'
                },

            })
})();
