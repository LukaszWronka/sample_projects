(function () {

    'use strict';

    angular
        .module('sample_project')
        .factory('notificationsService', ['Restangular','$q','$timeout',service]);

    function service(Restangular,$q,$timeout) {

        // var taskDictionary = dictionaries.getFor('task').task;
        var notifications=Restangular.all('notifications') ;
        var notificationsConfig = Restangular.all('notificationConfigs');
        var notificationCollection = [];




        var service = {
            //    taskDictionary : taskDictionary,
            getNotificationsForUser : getNotificationsForUser,
            createNotificationForTask : createNotificationForTask,
            notificationCollection : notificationCollection,
            getNotificationsByUserIdIsRead : getNotificationsByUserIdIsRead,
            setIsRead : setIsRead,
            notificationToRestangular : notificationToRestangular,


        };

        return service;

        function getNotificationsByUserIdIsRead(id){
            return notifications
                    .customGET('search/findByIsReadAndUserIdIn',{isRead:false,userIds:id});
        }

        function setIsRead(data){
            //return notifications.post(data);
            return data.put();
        }
        function notificationToRestangular(element) {
            return Restangular.restangularizeElement(null, element, 'notifications');
        }

        function getNotificationsForUser(id) {
            notificationCollection = [];
            return notifications
                .customGET('search/findByUserIdIn', { userId : id})
                .then(resolveFetchedNotification)
                .then(function (notifications) {

                    var promises = [];

                    _.forEach(notifications,function (element, index) {
                        var deffered = $q.defer();
                        promises.push(deffered.promise);
                        element.one('task').get().then(function (task) {
                            if(element.isRead == 0)
                            {
                                notificationCollection.push(task);

                            }
                            deffered.resolve();
                        })
                    });

                   return $q.all(promises).then(function () {
                       var deferred = $q.defer();
                       deferred.resolve(notificationCollection);
                       return deferred.promise

                    });
                });
        }

        function resolveFetchedNotification(data) {
            if(_.isArray(data.notifications))
            {
                return Restangular.restangularizeCollection(null,data.notifications,'notifications');
            }
            else $q.reject('error has accured');


        }

        function createNotificationForTask(taskLink) {
            var data = {
                priority : 10,
                isNotified : 0,
                task : taskLink

            }
            //return notificationsConfig.post(data);
            return notifications.post(data);
        };

    }

}());
