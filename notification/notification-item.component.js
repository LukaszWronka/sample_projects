
(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('aqNotificationItem', {
            templateUrl: 'app/partials/notification/notification-item.view.html',
            controller: controller,
            bindings: {
                item: '=',
                onMarkAsDone : '&',

            }
        });

    function controller($q,$scope,notificationsService,dictionariesService,NgTableParams,LoggedUserService,$interval, $state) {
        var ctrl = this;
        ctrl.markAsDone = markAsDone;
        ctrl.showNewPage = showNewPage;
        ctrl.notificationsRead = false;
        ctrl.setPropertyIsRead = setPropertyIsRead;

        ctrl.substitutionsStyle = {
        "background-color" : "#F8F8F8",
        "margin-left" : "0px",
        "margin-right" : "0px",

        }


        function showNewPage(event) {
            if (event.relatedTo == "CLIENT") {
                return $state.go("index.clients.detail", ({clientId : event.relatedToId}));
            }
            else if (event.relatedTo == "EVENT") {

                return $state.go("index.events.new.details", ({eventId : event.relatedToId}));
            }
            else if (event.relatedTo == "CONTRACT") {
                return $state.go("index.clients.detail.contracts.detail", ({clientId : event.relatedToId}));
            }
            else {
                    return $state.go("index");
            }

        }

        function setPropertyIsRead(notifications){
            setTimeout(function(){
                console.log("setPropertyIsRead",notifications);
                   notifications.isRead = true;
                   var temp = notificationsService
                                .notificationToRestangular(notifications);
                   notificationsService
                            .setIsRead(temp)
                            .then(function(data){
                                    ctrl.onMarkAsDone(data);
                                    });

            }, 500);

        }


        function markAsDone(task) {

            var taskLink = task._links.self.href;

            task.one('notifications').getList().then(function (notifications) {


                var promises = [];
                notifications.forEach(function (item, index) {
                    var deferred = $q.defer();

                    promises.push(deferred.promise);

                    item.isRead = true;
                    item.task = taskLink;
                    item.put().then(function (result) {
                        deferred.resolve();
                    });
                });

                return $q.all(promises).then(function () {
                    var deferred = $q.defer();
                    deferred.resolve('done');
                    return deferred.promise;

                });

            }).then(function (result) {
                ctrl.onMarkAsDone({});
            });

            //ctrl.onMarkAsDone({item : notification});

        }






    }

}());
