
(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('aqNotificationsDropdown', {
            templateUrl: 'app/partials/notification/notifications-dropdown.view.html',
            controller: controller,
            bindings: {
                elementResource: '<',
            }
        });

    function controller($scope,notificationsService,dictionariesService,NgTableParams,$interval,LoggedUserService,$state, Restangular, $timeout,$q) {



        var ctrl = this;
        ctrl.removeElement = removeElement;
        ctrl.showNewPage = showNewPage;
        ctrl.refreshData = refreshData;
        ctrl.notificationLastAmount = 0;
        ctrl.notification = [];
        ctrl.bellShake = false;
        ctrl.bool = true;
        ctrl.actualNotifications = []


        function refreshData () {
                notificationsService.getNotificationsByUserIdIsRead(LoggedUserService.getUserId()).then(function(data){
                    ctrl.notification = data;
                    ctrl.actualNotifications = data.notifications;
                    var promises = [];


                    if (!_.isEmpty(LoggedUserService.getSubstitutions())) {
                        _.each(LoggedUserService.getSubstitutions(),  function(id){
                            var deferred = $q.defer();
                             promises.push(deferred.promise);

                             notificationsService
                                .getNotificationsByUserIdIsRead(id)
                                .then(function(data){
                                    deferred.resolve(data);
                                })
                                .catch(function(error) {
                                    deferred.reject(error);
                                })
                        })

                        $q.all(promises).then(function(data) {

                            _.each(data, function(item){
                                !_.isEmpty(item.notifications) ? _.each(item.notifications, function(item){
                                    item.substitutions = true;
                                    ctrl.actualNotifications.push(item)}) : console.log();
                            })

                        });
                    }
                    loadCollectionToScope(ctrl.actualNotifications);
                    if(ctrl.actualNotifications.length>ctrl.notificationLastAmount ){
                        ctrl.bellShake = true;
                    }
                    else {
                        ctrl.bellShake = false;
                    }
                    ctrl.notificationLastAmount = ctrl.actualNotifications.length;
                    console.log(ctrl.actualNotifications);
                })


        }
        refreshData();
        $interval(refreshData, 70000);



        function showNewPage(event) {
            if (event.relatedTo == "CLIENT") {
                return $state.go("index.clients.detail", ({clientId : event.relatedToId}));
            }
            else if (event.relatedTo == "EVENT") {

                return $state.go("index.events.new.details", ({eventId : event.relatedToId}));
            }
            else if (event.relatedTo == "CONTRACT") {
                return $state.go("index.clients.detail.contracts", ({clientId : event.relatedToId}));
            }
            else {
                    return $state.go("index");
            }

        }



        function removeElement(idx) {
            ctrl.actualNotifications.splice(idx,1);

        }
        function loadCollectionToScope(collection) {
            ctrl.notificationCollection = collection;
        }


    }

}());
