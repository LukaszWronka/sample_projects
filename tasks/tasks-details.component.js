(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('tasksDetails', {
            templateUrl: 'app/partials/tasks/tasks-details.view.html',
            controller: controller,
            require: {
                taskCtrl: '^tasks'
            },
            bindings : {
                detailedTask : '<',
                taskConfigCollection : '<'
            }
        });

    controller.$inject = ['$scope', '$uibModal', 'toaster','$state','taskService'];
    function controller($scope,$uibModal,toaster,$state,taskService) {
        var ctrl = this;
        ctrl.cancelTask = cancelTask;
        ctrl.markAsResolved = markAsResolved;

        function cancelTask(task){
            task.status = "CANCELLED";
            task.put().then(function (data) {
                toaster.pop('success', "Success", "Task was cannceled");
                ctrl.taskCtrl.updateTaskCollection(task);
            });
        }
        function markAsResolved(task){
            task.status = "DONE";
            task.put().then(function(data){
                toaster.pop('success', "Success", "Task was marked as a done");
                ctrl.taskCtrl.updateTaskCollection(task);
            });
        }


    }
}());
