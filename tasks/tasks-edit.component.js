(function() {
    "use strict";

    angular
        .module('sample_project')
        .component('tasksEdit', {
            templateUrl: 'app/partials/tasks/tasks-edit.view.html',
            controller: controller,
            bindings: {
                detailedTask: '<',
                taskConfigCollection: '<'
            }
        });

    controller.$inject = ['$scope', '$uibModal', 'toaster', '$state', 'taskService', 'dictionariesService', 'Restangular'];

    function controller($scope, $uibModal, toaster, $state, taskService, dictionariesService, Restangular) {
        var ctrl = this;


        ctrl.showDeadlineCalendar = showDeadlineCalendar;
        ctrl.activate = activate;
        ctrl.get = get;
        ctrl.addTimeAlert = addTimeAlert;
        ctrl.showTimeAlertCalendar = showTimeAlertCalendar;
        ctrl.saveEdit = saveEdit;
        ctrl.createIsoDateTimeFromInputs = createIsoDateTimeFromInputs;
        ctrl.remove = remove;
        ctrl.relationSelect = relationSelect;
        ctrl.selectedRelation = selectedRelation;
        ctrl.contract = {};





        ctrl.isOpenDeadlineDatapicker = false;
        ctrl.taskDictionary = dictionariesService.getFor('task');
        ctrl.detailedTask.deadlineDate = ctrl.detailedTask.deadlineTime = new Date(ctrl.detailedTask.deadline);
        ctrl.operators = null;
        ctrl.removeDisabled = [];
        ctrl.alertStyle = true;


        ctrl.timeAlertArray = [];
        ctrl.isOpenTimeAlertDatapicker = [];
        ctrl.relationsArray = [];

        ctrl.slider = {
            value: 10,
            options: {
                showSelectionBar: true,
                getSelectionBarColor: function(value) {
                    if (value <= 3)
                        return 'green';
                    if (value <= 6)
                        return 'orange';
                    if (value <= 8)
                        return 'red';
                    return 'red';
                }
            }
        };


        this.$onInit = function () {

            switch(ctrl.detailedTask.relatedTo) {
                        case 'CONTRACT':
                            getRelatedData(ctrl.detailedTask.relatedTo);
                            taskService.getContracts(ctrl.detailedTask.relatedToId).then(function (contract) {
                             ctrl.contract.selected = {id: contract.id};
                            })
                            break;
                        case 'CLIENT':
                            getRelatedData(ctrl.detailedTask.relatedTo);
                            taskService.getClients(ctrl.detailedTask.relatedToId).then(function(clients){
                                ctrl.contract.selected = {firstName: clients.firstName, surname : clients.surname, companyName : clients.companyName};
                            })

                            break;
                        case 'EVENT':
                        getRelatedData(ctrl.detailedTask.relatedTo);
                        taskService.getEvent(ctrl.detailedTask.relatedToId).then(function(events){
                            ctrl.contract.selected = {id: events.id};
                        })
                            break;
                        default:
                            break;
                    }

        };


        activate();

        function showDeadlineCalendar(val) {
            ctrl.isOpenDeadlineDatapicker = val;
        }

        function activate() {
            ctrl.get();
            taskService.getOperators().then(function(data) {
                ctrl.operators = data;
            });
            taskService.getUserLink(ctrl.detailedTask.id).then(function(data) {
                ctrl.operator = data._links.self.href;
            })
            _.forEach(ctrl.taskConfigCollection, function(row, index) {
                ctrl.timeAlertArray.push({
                    isMain: true
                });
                ctrl.removeDisabled.push({
                    isMain: true
                })
                ctrl.taskConfigCollection[index].timeAlertDate = new Date(row.remindDate);
                ctrl.taskConfigCollection[index].timeAlertTime = row.remindDate;
                var now = moment();
                if (moment(moment(row.remindDate)).isAfter(now._d)) {
                    ctrl.removeDisabled[index].isMain = false;
                }


            });
        }

        function get() {
            ctrl.detailedTask.one('user').get().then(function(data) {
                ctrl.detailedTask.user = data.name + " " + data.surname;
            })

        }

        function addTimeAlert() {
            ctrl.alertStyle = false;
            var isMain = false;
            if (ctrl.timeAlertArray.length === 0) {
                isMain = true;
            }
            ctrl.timeAlertArray.push({
                isMain: isMain
            });
        }

        function showTimeAlertCalendar(idx, val) {
            ctrl.isOpenTimeAlertDatapicker[idx] = val;
        }

        function saveEdit() {
            ctrl.detailedTask.user = ctrl.operator;
            ctrl.detailedTask.put().then(function(data) {
                _.forEach(ctrl.taskConfigCollection, function(row, index) {

                    row.remindDate = createIsoDateTimeFromInputs(row.timeAlertDate, row.timeAlertTime);

                    if (row.restangularized) {
                        row.put();
                    } else {
                        row.task = ctrl.detailedTask._links.self.href;
                        taskService.postTaskConfing(row);
                    }

                });
            }).then(function() {
                toaster.pop('success', "Success", "Change was added added");
            })
        }

        function remove(index) {
            if (ctrl.timeAlertArray.length === 1) {
                ctrl.alertStyle = true;
            }
            ctrl.timeAlertArray.splice(index, 1);
            if (_.has(ctrl.taskConfigCollection[index], 'id')) {
                ctrl.taskConfigCollection[index].remove().then(showSuccessMessage);
            }
            ctrl.taskConfigCollection.splice(index, 1);
        }

        function showSuccessMessage(data) {
            toaster.pop('success', "Success", "Time alert was removed");
            return this;
        }

        function createIsoDateTimeFromInputs(deadlineDate, deadlineTime) {
            var date = moment(deadlineDate).format('DD-MM-YYYY');
            var time = moment(deadlineTime).format('HH:mm');
            var deadlineDateTime = moment(date + " " + time, "DD-MM-YYYY HH:mm");
            return deadlineDateTime.toISOString();

        }


        function relationSelect(relation) {

            getRelatedData(relation);

        }

        function selectedRelation (relation){
            ctrl.detailedTask.relatedToId = relation.id;
        }


        function getRelatedData(relation){

            switch(relation) {
                case 'CLIENT':
                taskService.getClients().then(function(clients){
                    ctrl.relationsArray = clients;
                })
                    break;
                case 'EVENT':
                taskService.getEvent().then(function(events){
                    ctrl.relationsArray = events;
                })
                    break;
                case 'CONTRACT':
                taskService.getContracts().then(function(contracts){
                    ctrl.relationsArray = contracts;
                })
                    break;
                default:
                    break;
            }

        }

    }
}());
