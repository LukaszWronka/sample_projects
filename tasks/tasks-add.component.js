(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('tasksAdd', {
            templateUrl: 'app/partials/tasks/tasks-add.view.html',
            controller: controller,
            require: {
                taskCtrl: '^tasks'
            },
            bindings : {
                  userRole : "<",
            }
        });

    controller.$inject = ['$scope', '$uibModal', 'toaster','$state','taskService','dictionariesService','LoggedUserService'];
    function controller($scope,$uibModal,toaster,$state,taskService,dictionariesService,LoggedUserService,loggedUser) {
        var ctrl = this;



        ctrl.showDeadlineCalendar = showDeadlineCalendar;
        ctrl.activate = activate;
        ctrl.addTimeAlert = addTimeAlert;
        ctrl.saveNew = saveNew;
        ctrl.showSuccessMessage = showSuccessMessage;
        ctrl.showTimeAlertCalendar = showTimeAlertCalendar;
        ctrl.postTaskConfing = postTaskConfing;
        ctrl.clearForm = clearForm;
        ctrl.remove = remove;
        ctrl.relationSelect = relationSelect;
        ctrl.selectedRelation = selectedRelation;

        ctrl.timeAlertArray = [];
        ctrl.isOpenTimeAlertDatapicker = [];

        ctrl.taskDictionary = dictionariesService.getFor('task');
        ctrl.isOpenDeadlineDatapicker = false;
        ctrl.alertStyle = true;
        ctrl.newTask = {};
        ctrl.taskConfigData = {};
        ctrl.newTask.status = "TODO";
        ctrl.newTask.source = "USER";
        ctrl.newTask.relatedToId ;
        ctrl.taskLink;
        ctrl.taskConfingsDate = {} ;
        ctrl.taskConfingsDate.timeAlertDate = [];
        ctrl.taskConfingsDate.timeAlertTime = [];
        ctrl.relationsArray = [];


        ctrl.slider = {
            value: 10,
            options: {
                showSelectionBar: true,
                getSelectionBarColor: function(value) {
                    if (value <= 3)
                        return 'green';
                    if (value <= 6)
                        return 'orange';
                    if (value <= 8)
                        return 'red';
                    return 'red';
                }
            }
        };


        activate();

        function showDeadlineCalendar(val) {
            ctrl.isOpenDeadlineDatapicker = val;
        }

        function activate() {
            taskService.getOperators().then(function(data) {
                ctrl.operators = data;
            });
        }

        function addTimeAlert() {
            var isMain = false;
            ctrl.alertStyle = false;
            if (ctrl.timeAlertArray.length === 0) {
                isMain = true;
            }
            ctrl.timeAlertArray.push({isMain: isMain});
        }

        function saveNew() {
            ctrl.newTask.deadline = createIsoDateTimeFromInputs(ctrl.newTask.deadlineDate, ctrl.newTask.deadlineTime);
            ctrl.newTask.priority = ctrl.slider.value;
            if (!LoggedUserService.getLoggedUser().isAdmin()) {
              ctrl.newTask.user = LoggedUserService.getLoggedUser()._links.self.href;

            }
            taskService.post(ctrl.newTask)
                .then(function(task){
                    ctrl.alertStyle = true;
                    ctrl.taskLink = task._links.self.href;
                    if (task.relatedToId == null) {
                        task.relatedToId = task.id;
                        task.put();
                    }
                    ctrl.taskCtrl.addElementTaskCollection(task);
                })
                .then(showSuccessMessage)
                .then(postTaskConfing)
                .then(clearForm)
        }

        function createIsoDateTimeFromInputs(deadlineDate, deadlineTime) {
            var date = moment(deadlineDate).format('DD-MM-YYYY');
            var time = moment(deadlineTime).format('HH:mm');
            var deadlineDateTime =   moment(date+" "+time, "DD-MM-YYYY HH:mm");
            return deadlineDateTime.toISOString();

        }
        function showSuccessMessage(data) {
            toaster.pop('success', "Success", "Task was added");
            return this;
        }
        function postTaskConfing() {
            _.forEach(ctrl.taskConfingsDate.timeAlertDate, function(row, index){
                row.task = ctrl.taskLink;
                row.remindDate = createIsoDateTimeFromInputs(row.timeAlertDate,ctrl.taskConfingsDate.timeAlertTime[index]);
                row = {task :ctrl.taskLink,
                        remindDate : createIsoDateTimeFromInputs(row.timeAlertDate,ctrl.taskConfingsDate.timeAlertTime[index])}
                taskService.postTaskConfing(row);
            })
        }
        function showTimeAlertCalendar(idx,val) {
            ctrl.isOpenTimeAlertDatapicker[idx] = val;
        }
        function clearForm() {
            ctrl.newTask.title="";
            ctrl.newTask.description="";
            ctrl.newTask.type="";
            ctrl.newTask.notifications="";
            ctrl.newTask.relatedTo="";
            ctrl.newTask.deadlineDate="";
            ctrl.newTask.deadlineTime="";
            ctrl.newTask.user="";
            ctrl.slider.value = 10;
            ctrl.timeAlertArray = [];
            ctrl.taskConfingsDate = {};
            ctrl.taskConfingsDate.timeAlertDate = [];
            ctrl.taskConfingsDate.timeAlertTime = [];
        }

        function remove(index){
            if (ctrl.timeAlertArray.length === 1) {
                   ctrl.alertStyle = true;
                }
            ctrl.timeAlertArray.splice(index,1);
            console.log(ctrl.taskConfingsDate);

            ctrl.taskConfingsDate.timeAlertDate.splice(index,1);
            ctrl.taskConfingsDate.timeAlertTime.splice(index,1);

        }

        function relationSelect(relation) {


            if (relation == "CLIENT") {
                taskService.getClients().then(function(clients){
                    ctrl.relationsArray = clients;
                })
            }
            else if (relation == "EVENT") {
                taskService.getEvent().then(function(events){
                    ctrl.relationsArray = events;
                })

            }
            else if (relation == "CONTRACT") {
                taskService.getContracts().then(function(contracts){
                    ctrl.relationsArray = contracts;
                })
            }
            else if(relation == "TASK") {
                ctrl.newTask.relatedToId = null;
            }
        }

        function selectedRelation (relation){
            ctrl.newTask.relatedToId = relation.id;
        }



    }
}());
