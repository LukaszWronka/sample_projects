
(function () {
    "use strict";

    angular
        .module('sample_project')
        .component('tasks', {
            templateUrl: 'app/partials/tasks/tasks.view.html',
            controller: controller,
            bindings: {

                taskCollection : '<',
                userRole : '<',
            }
        });

    function controller($q,$scope,notificationsService,taskService,NgTableParams, LoggedUserService,toaster) {
        var ctrl = this;

        ctrl.get = get;
        ctrl.getUser = getUser;
        ctrl.showTaskDetails = showTaskDetails;
        ctrl.getTask = getTask;
        ctrl.cancelTask = cancelTask;
        ctrl.markAsResolved = markAsResolved;
        ctrl.addElementTaskCollection = addElementTaskCollection;
        ctrl.updateTaskCollection = updateTaskCollection;
        ctrl.taskFlitrationCollection = [];

        this.$onInit = function() {
                get();
            };


        function get() {
            var promises = [];
            _.each(LoggedUserService.getSubstitutions(), function(id){
                var deferred = $q.defer();
                promises.push(deferred.promise);
                taskService
                    .get(id)
                    .then(function(task){
                        deferred.resolve(task);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
            })

             $q.all(promises)
                .then(function(data){
                    _.each(data, function(task){
                        ctrl.taskCollection.push(task);
                    })
                })

            if (LoggedUserService.getLoggedUser().isAdmin()) {
              _.forEach(ctrl.taskCollection,function (row, index) {
                 row.one('user').get().then(function (data) {
                         row.operator = data.name+" "+data.surname;
                 })

             });
            }
            else {
              ctrl.taskCollection = ctrl.taskCollection.tasks;
            }
        }


        function getUser(){
            return LoggedUserService.getLoggedUser();
        }
        function getTask(task){
            return task;
        }

        function showTaskDetails(task){
            console.log(task);
        }
        function cancelTask(task){
            task.status = "CANCELLED";
                var taskRest = taskService
                                .elementToRestangularTask(task);

                taskRest.put()
                    .then(toaster.pop('success', "Success", "Task was cannceled"));

        }
        function markAsResolved(task){
            task.status = "DONE";
            var taskRest = taskService
                                .elementToRestangularTask(task);

                taskRest.put()
                    .then(toaster.pop('success', "Success", "Task was marked as a done"));

        }

        function getTask(selectTask, user) {
            selectTask.user = user._links.self.href;
            selectTask.put()
                        .then(function () {
                            toaster.pop('success', "Success", "Operator was changed");
                        })
                        .then(get)
                    }

        function addElementTaskCollection(task) {

            task.one('user').get()
                .then(function (data) {
                        task.operator = data.name+" "+data.surname;
                        ctrl.taskCollection.push(task);
            })
        }
        function updateTaskCollection(task){
            _.forEach(ctrl.taskCollection, function(row, index){
                if (row.id == task.id) {
                    row.status = task.status;
                }
            })
        }
    }

}());
