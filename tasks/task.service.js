(function () {

    'use strict';

    angular
        .module('sample_project')
        .factory('taskService', ['Restangular','$q','$timeout',controller]);

    function controller(Restangular, $q, $timeout) {


        var tasks = Restangular.all('tasks');
        var taskConf = Restangular.all('taskConfigs');
       // var taskDictionary = dictionaries.getFor('task').task;
        var service = {
        //    taskDictionary : taskDictionary,
            get : get,
            post : post,
            getOperators : getOperators,
            postTaskConfing : postTaskConfing,
            getTaskConfing : getTaskConfing,
            elementToRestangular : elementToRestangular,
            putTaskConfing : putTaskConfing,
            getUserLink : getUserLink,
            getTaskByUserId : getTaskByUserId,
            elementToRestangularTask : elementToRestangularTask,
            getClients : getClients,
            getContracts : getContracts,
            getEvent : getEvent,
            getTasksByRelatedToItem: getTasksByRelatedToItem,

        };

        return service;

        function get(id) {
             return !_.isUndefined(id) ? tasks.get(id) : tasks.getList();
        }

        function post(data) {
            return tasks.post(data);
        };

        function getOperators() {
          return  Restangular.all('users').getList();
        }

        function postTaskConfing(data){
            return taskConf.post(data);
        }
        function getTaskConfing(task) {
            return task.getList('taskConfigs');
        }
        function elementToRestangularTask(data) {
            return Restangular.restangularizeElement(null, data, 'task');
        }
        function elementToRestangular(data){
            console.log(data);
            return Restangular.restangularizeElement(null, data, 'taskConfigs');
        }
        function putTaskConfing(taskConf){
            return taskConf.put();
        }
        function getUserLink(id){
            return Restangular.one('tasks/'+id+'/user').get();
        }
        function getTaskByUserId(id) {
          return tasks
                  .customGET('search/findByUserId',{userId:id});
        }
        function getClients(id) {
            return !_.isUndefined(id) ?  Restangular.all('clients').get(id) :  Restangular.all('clients').getList();
        }
        function getContracts(id) {
            return !_.isUndefined(id) ?  Restangular.all('contracts').get(id) :  Restangular.all('contracts').getList();
        }
        function getEvent(id) {
            return !_.isUndefined(id) ?  Restangular.all('event').get(id) :  Restangular.all('event').getList();
        }


        function getTasksByRelatedToItem(item, id) {
            return Restangular
                    .all('tasks')
                    .customGET('search/findByRelatedToAndRelatedToId',{'relatedTo': item, 'relatedToId' : id})
                    .then(restangularizeTaskCollection);

        }

        function restangularizeTaskCollection(response) {
            return Restangular.restangularizeCollection(response, response.tasks, 'tasks');
        }







    }

}());
